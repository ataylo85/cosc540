from socket import create_connection
from ssl import SSLContext, PROTOCOL_TLS_CLIENT
import sys

hostname='example.org'
ip = '10.1.1.53'
port = 3000
context = SSLContext(PROTOCOL_TLS_CLIENT)
context.load_verify_locations('cert.pem')
FORMAT = 'ASCII'
HEADER = 64
DISCONNECT_MESSAGE = "x"

#this variable is for handling entry from file or user
TAKING_CODONS_INPUT = True

#port from start args or get input
if len(sys.argv) < 3:
    PORT = int(input("please define the port you would like to connect to"))
else:
    PORT = int(sys.argv[2])

#ip from start args or get input
if len(sys.argv) < 2:
    SERVER = input("please define the server you would like to connect to")
else:
    SERVER = sys.argv[1]

#send by file or prompt input
CODONS = None
if len(sys.argv) == 4:
    infile = open(sys.argv[3], 'r')
    CODONS = infile.read()
    print("here are the codons")
    print(CODONS)
    TAKING_CODONS_INPUT = False

#handles just the sending of the codon data, no validity checking
def send(msg, tls):
    print("sending " + msg)
    message = msg.encode(FORMAT)
    msg_length = len(message)
    #calculate length of string and send as header, instead of set START RNA message
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    tls.sendall(send_length)
    tls.sendall(message)
    response = tls.recv(2048).decode(FORMAT)
    if is_valid_codon(response) and msg != "x":
        print(response)

#put codons in a dictionary and use it for checking validity
def init_codon_dict(codon_info_csv):
    CODONS = dict()
    f = open(codon_info_csv)
    for line in f:
        line = line.strip('\n')
        (codon, protein) = line.split(",")
        CODONS[codon] = protein
    return CODONS

VALID_CODONS = init_codon_dict("codon-aminoacid.csv")

#check if the data entered is valid before sending and on receipt
def is_valid_codon(codon):
    print(codon)
    valid = True
    codons_length = len(codon)
    print("length:", codons_length)
    #no empties
    if codons_length == 0:
        valid = False
        print("Error - length of zero")
    if codons_length%3 != 0:
        valid = False
        print("Error - not divisible by 3")
    if valid:
        for x in range(int(codons_length/3)):
            start = x
            end = x+3
            if codon[start:end] not in VALID_CODONS:
                valid = False
                print("error - not valid codon")
    if valid:
        print("passed codon check")
        return True
    else:
        print("failed codon check")
        return False


with create_connection((ip, port)) as client:
    with context.wrap_socket(client, server_hostname=hostname) as tls:
        print(f'Using {tls.version()}\n')
        while TAKING_CODONS_INPUT:
            CODONS = input("Enter codons to optimize, or enter X to disconnect")
            CODONS = CODONS.upper()
            if (is_valid_codon(CODONS)):
                send(CODONS, tls)
            else:
                send(DISCONNECT_MESSAGE, tls)
                TAKING_CODONS_INPUT = False

