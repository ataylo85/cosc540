from socket import socket, AF_INET, SOCK_STREAM, gethostname,  gethostbyname
from ssl import SSLContext, PROTOCOL_TLS_SERVER
import threading
import sys

SERVER = gethostbyname(gethostname())
port = int(sys.argv[1])
context = SSLContext(PROTOCOL_TLS_SERVER)
context.load_cert_chain('cert.pem', 'key.pem')

HEADER = 64
FORMAT = "ASCII"



def is_valid_codon(codon):
    if codon in optimals:
        print(codon + " this codon is valid")
        return True
    else:
        print(codon + " this codon is NOT valid")
        return False


#need to score codons based on their efficiency (G or C is better)
def score_codon(codon):
    score = 0
    for letter in codon:
        if letter == "G" or letter == "C":
            score+=1
    return score

#create a data store as a dictionary, where we can look up the codon to return
def init_codon_dict(codon_info_csv):
    OPTIMAL_CODONS = dict()
    CODON_AS_KEY = dict()
    f = open(codon_info_csv)
    for line in f:
        line = line.strip('\n')
        (codon, protein) = line.split(",")
        if protein in OPTIMAL_CODONS:
            if score_codon(codon) > score_codon(OPTIMAL_CODONS[protein]):
                OPTIMAL_CODONS[protein] = codon
        else:
            OPTIMAL_CODONS[protein] = codon
        CODON_AS_KEY[codon] = protein
    for codon in CODON_AS_KEY:
        CODON_AS_KEY[codon] = OPTIMAL_CODONS[CODON_AS_KEY[codon]]
    return CODON_AS_KEY

optimals = init_codon_dict("codon-aminoacid.csv")

#this is where the ip address would get validated. 
#I have not implemented any validation, so just return true for everyone.

def is_whitelisted_ip(ip_address):
    return True

def handle_client(conn, addr):
    connected = True
    print(f'Connected by {addr}\n')
    print("checking IP address")

    if is_whitelisted_ip(addr):
        print("IP validated")
    else:
        print("Unauthorised user... disconnecting...")
        connected = False

    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            print(msg)
            if msg == "x":
                connected = False
            else:
                if msg_length%3 != 0:
                    connected = False
                else:
                    codons_all_valid = True
                    msg = msg.upper()
                    #validate codons: length of string should be multiple of three, all codons should be valid
                    startpoint = 0
                    endpoint = 3
                    result = ""
                    while endpoint <= msg_length:
                        print(endpoint, " ? ", msg_length)
                        checking = msg[startpoint:endpoint]
                        if is_valid_codon(checking):
                            print(checking)
                            result += optimals[checking]
                        else:
                            connected = False
                            codons_all_valid = False
                            print("should exit here")
                        
                        startpoint+=3
                        endpoint+=3
                    print("finished checking all codons")
                    if codons_all_valid:
                        conn.send(result.encode(FORMAT))
                    else:
                        connected = False
        else:
            connected = False
    conn.close()



with socket(AF_INET, SOCK_STREAM) as server:
    server.bind((SERVER, port))
    server.listen()
    with context.wrap_socket(server, server_side=True) as tls:
        connection, address = tls.accept()
        thread = threading.Thread(target=handle_client, args=(connection, address))
        thread.start()



